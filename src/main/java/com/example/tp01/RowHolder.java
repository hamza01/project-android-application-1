package com.example.tp01;

import android.content.Intent;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.RecyclerView;

public class RowHolder extends RecyclerView.ViewHolder implements View.OnClickListener {


        TextView label=null;
        ImageView icon=null;

        RowHolder(View row) {
            super(row);

            label=(TextView)row.findViewById(R.id.label);
            icon=(ImageView)row.findViewById(R.id.imageViewRow);
            this.itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v){
            final String item = label.getText().toString();

            Animal animal = AnimalList.getAnimal(item);
            Intent intent = new Intent(this.itemView.getContext(), ActiveMain.class);
            intent.putExtra("nomAnimal", item);
            this.itemView.getContext().startActivity(intent);
            Toast.makeText(v.getContext(), "You select : " + item, Toast.LENGTH_LONG).show();
        }

        void bindModel(String item) {
            Animal animal = AnimalList.getAnimal(item);
            label.setText(item);
            icon.setImageResource(this.itemView.getContext().getResources().getIdentifier(animal.getImgFile(), "drawable",this.itemView.getContext().getPackageName()));

        }
    }

