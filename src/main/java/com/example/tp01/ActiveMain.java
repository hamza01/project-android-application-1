package com.example.tp01;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import org.w3c.dom.Text;

public class ActiveMain extends AppCompatActivity {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.listx);
        Intent intent = getIntent();
        String msg = intent.getStringExtra("nomAnimal");
       final Animal animal = AnimalList.getAnimal(msg) ;


        String foto = animal.getImgFile() ;
        Context context = getApplicationContext() ;
        int imgId = context.getResources().getIdentifier(foto, "drawable",context.getPackageName()) ;
        try {
            ImageView image = findViewById(R.id.picture);
            image.setImageResource(imgId);
        }
        catch(Exception e){
            System.out.println(imgId);
            e.printStackTrace();
        }
       /* ImageView esperance0 = findViewById(R.id.image);
        esperance0.setImageResource(animal.getImgFile()); */

        TextView titre1 = findViewById(R.id.title);
        titre1.setText(msg);

        TextView esperance = findViewById(R.id.espAnimal);
        esperance.setText(animal.getStrHightestLifespan());
        TextView gestationPer = findViewById(R.id.perAnimal);
        gestationPer.setText(animal.getStrGestationPeriod());
        TextView poidNaissance = findViewById(R.id.poidsAnimal);
        poidNaissance.setText(animal.getStrBirthWeight());
        TextView poidAdult = findViewById(R.id.poidsAnimalAdulte);
        poidAdult.setText(animal.getStrAdultWeight());
        final EditText conservation = findViewById(R.id.statutAnimal);
        conservation.setText(animal.getConservationStatus());

        Button b = findViewById(R.id.save);
        b.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                    String conservationText = conservation.getText().toString();
                    animal.setConservationStatus(conservationText);
            }
        });

    }



}
